
-- register the api for the async environment if it exists
if minetest.register_mapgen_script then
    minetest.register_mapgen_script(minetest.get_modpath("biomegen").."/mapgen.lua")
end
-- register the api for the regular environment regardless
dofile(minetest.get_modpath("biomegen").."/mapgen.lua")
